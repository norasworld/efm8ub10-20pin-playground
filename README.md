# An experimenting and learning board for the Silicon Labs EFM8UB10

## Variants
- [2layer](./efm8ub10-20pin-playground-2layer/) This version is "standard" for having boards made and should be cheap with most board houses. Since it is as close to the millable version as possible, the in-copper markings are there as well as markings in the silkscreen.
- [mill](./efm8ub10-20pin-playground-mill/) This version is for milling the board yourself, it is drawn to not waste too many expensive mills on the local fablab's LPKF S-63, and uses rivets (0.4mm inner diameter, either from Bungard or Voltera) for vias.

## Infos
The board can have a shroud glued on to protect the pinheader, the relevant files are in [./pinheader-shroud/](pinheader-shroud).

## Status
As of now (2024-05-20) the first boards are in production, so everything is UNTESTED.
