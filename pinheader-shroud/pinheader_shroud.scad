// upper shroud for horizontal 2x06 SMD pinheader

$fn = 16;
//$fn=64;

plastic_colour = [0.6, 0.6, 1];

rotate([0, 0, 90])
union() {
	color(plastic_colour) {
		difference() {
			hull() {
				// top side
				translate([7.36-2.54, 6.985, 5.2+1-0.8]) rotate_extrude() translate([2.54-0.8, 0]) circle(r=0.8);
				translate([7.36-2.54, -6.985, 5.2+1-0.8]) rotate_extrude() translate([2.54-0.8, 0]) circle(r=0.8);
				translate([-10.16+2+2.54, 6.985, 5.2+1-0.8]) rotate_extrude() translate([2.54-0.8, 0]) circle(r=0.8);
				translate([-10.16+2+2.54, -6.985, 5.2+1-0.8]) rotate_extrude() translate([2.54-0.8, 0]) circle(r=0.8);
				// bottom side
				translate([7.36-2.54, 6.985, 0]) rotate_extrude() translate([2.54-0.8, 0]) circle(r=0.8);
				translate([7.36-2.54, -6.985, 0]) rotate_extrude() translate([2.54-0.8, 0]) circle(r=0.8);
				translate([-10.16+2.54, 6.985, 0]) rotate_extrude() translate([2.54-0.8, 0]) circle(r=0.8);
				translate([-10.16+2.54, -6.985, 0]) rotate_extrude() translate([2.54-0.8, 0]) circle(r=0.8);
			}
			// remove underside
			translate([0, 0, -10]) cube([30, 30, 20], true);
			// remove space for plastic part of the jack
			translate([1.27-4.2+10, 0, 2.599]) cube([20, 6*2.54+0.4, 5.2], true);
			// remove space for plug
			translate([1.27+10, 0, 2.599]) cube([20, 50, 5.2], true);
			// remove space for back pins
			for(i=[-5:2:5]) {
				translate([0, i*1.27, 0]) hull() {
					translate([-9.652+0.55, 0, 0]) sphere(d=1.3);
					translate([5, 0, 0]) sphere(d=1.3);
					translate([-9.652+2+0.65, 0, 3.81]) sphere(d=1.3);
					translate([5, 0, 3.81]) sphere(d=1.3);
				}
			}
			// text in top side
			// bottom pin row
			/*translate([5, -5*1.27, 6]) rotate([0, 0, 90]) linear_extrude(1) text(text="G", size=2.5, halign="center");
			translate([5, -3*1.27, 6]) rotate([0, 0, 90]) linear_extrude(1) text(text="7", size=2.5, halign="center");
			translate([5, -1*1.27, 6]) rotate([0, 0, 90]) linear_extrude(1) text(text="5", size=2.5, halign="center");
			translate([5, 1*1.27, 6]) rotate([0, 0, 90]) linear_extrude(1) text(text="3", size=2.5, halign="center");
			translate([5, 3*1.27, 6]) rotate([0, 0, 90]) linear_extrude(1) text(text="1", size=2.5, halign="center");
			translate([5, 5*1.27, 6]) rotate([0, 0, 90]) linear_extrude(1) text(text="+", size=2.5, halign="center");
			// top pin row
			translate([1.5, -5*1.27, 6]) rotate([0, 0, 90]) linear_extrude(1) text(text="G", size=2.5, halign="center");
			translate([1.5, -3*1.27, 6]) rotate([0, 0, 90]) linear_extrude(1) text(text="6", size=2.5, halign="center");
			translate([1.5, -1*1.27, 6]) rotate([0, 0, 90]) linear_extrude(1) text(text="4", size=2.5, halign="center");
			translate([1.5, 1*1.27, 6]) rotate([0, 0, 90]) linear_extrude(1) text(text="2", size=2.5, halign="center");
			translate([1.5, 3*1.27, 6]) rotate([0, 0, 90]) linear_extrude(1) text(text="0", size=2.5, halign="center");
			translate([1.5, 5*1.27, 6]) rotate([0, 0, 90]) linear_extrude(1) text(text="+", size=2.5, halign="center");*/
		}
	}
	translate([0, 0, 5.6]) cube([0.1, 0.1, 0.1], true);
}